import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavLeftbarComponent } from './nav-leftbar.component';

describe('NavLeftbarComponent', () => {
  let component: NavLeftbarComponent;
  let fixture: ComponentFixture<NavLeftbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavLeftbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavLeftbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
