import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SettingMenuComponent } from './setting-menu/setting-menu.component';
import { LeftSidebarComponent } from './left-sidebar/left-sidebar.component';
import { MainSidebarComponent } from './main-sidebar/main-sidebar.component';
import { RightSidebarComponent } from './right-sidebar/right-sidebar.component';
import { NavLeftbarComponent } from './nav-leftbar/nav-leftbar.component';

@NgModule({
  declarations: [
    AppComponent,
    SettingMenuComponent,
    LeftSidebarComponent,
    MainSidebarComponent,
    RightSidebarComponent,
    NavLeftbarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
